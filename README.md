# replaycap

A tool to replay pcap files to a network interface.  
The focus of packet timing and accuracy has been dropped in favour of reading  
what files to play from stdin, looping files, parallel sending of files and  
rate limiting.

Inspired by https://github.com/google/gopacket/blob/master/examples/pcaplay/main.go

```
./replaycap --help
Usage of ./replaycap:
  -bpf string
        BPF filter for written pcaps
  -i string
        Interface to write packets to (default "eth0")
  -limit string
        Bits / seconds limit on throughput (number can end with K, M, G, T) (default "0")
  -parallel int
        Number of pcaps that can be written to interface simultaneously in stdin mode (default 10)
  -repeat
        Repeat pcap output in both single pcap mode and stdin mode
  -single-file string
        Filename to read from
  -stdin
        Read pcap filenames from stdin, line seperated
```