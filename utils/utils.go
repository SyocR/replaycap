package utils

import (
	"bufio"
	"fmt"
	"os"
	"strconv"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func ReadStdin(input chan string) {
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		path := scanner.Text()
		if scanner.Err() != nil {
			log.Error().Err(scanner.Err()).Msg("Failed to read path from stdin")
		}
		input <- path
	}
	close(input)
}

func HumanBits(bits uint64) (fString string) {
	unit := []string{"", "K", "M", "G", "T"}
	index := 0

	fString = fmt.Sprintf("%sb", unit[0])
	for bits/1000 > 0 && index <= len(unit) {
		index++
		fString = fmt.Sprintf("%.3f %sb", float64(bits)/1000, unit[index])
		bits = bits / 1000
	}

	return fString
}

func FormatThroughput(bits uint64) (fString string) {
	fString = HumanBits(bits)
	fString = fmt.Sprintf("%s/s", fString)

	return fString
}

func SItoBits(bitstring string) (bits uint64, err error) {
	unit := string(bitstring[len(bitstring)-1])
	num := bitstring[:len(bitstring)-1]

	switch {
	case unit == "K":
		parsed, err := strconv.ParseUint(num, 10, 64)
		if err != nil {
			return 0, err
		} else {
			return parsed * 1000, nil
		}
	case unit == "M":
		parsed, err := strconv.ParseUint(num, 10, 64)
		if err != nil {
			return 0, err
		} else {
			return parsed * 1000 * 1000, nil
		}
	case unit == "G":
		parsed, err := strconv.ParseUint(num, 10, 64)
		if err != nil {
			return 0, err
		} else {
			return parsed * 1000 * 1000 * 1000, nil
		}
	case unit == "T":
		parsed, err := strconv.ParseUint(num, 10, 64)
		if err != nil {
			return 0, err
		} else {
			return parsed * 1000 * 1000 * 1000 * 1000, nil
		}
	default: // input is bits with no unit spesification
		parsed, err := strconv.ParseUint(bitstring, 10, 64)
		if err != nil {
			return 0, err
		} else {
			return parsed, nil
		}
	}
}

func SetVerbosity(verbosity string) {
	switch {
	case verbosity == "trace":
		zerolog.SetGlobalLevel(zerolog.TraceLevel)
	case verbosity == "info":
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	case verbosity == "debug":
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	case verbosity == "warning":
		zerolog.SetGlobalLevel(zerolog.WarnLevel)
	case verbosity == "error":
		zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	default:
		log.Fatal().Msg("the -v flag must be one of: trace,info,debug,warning,error")
	}
}
