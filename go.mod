module gitlab.com/SyocR/replaycap

go 1.13

require (
	github.com/Syoc/gopacket v1.2.4
	github.com/google/gopacket v1.1.19
	github.com/rs/zerolog v1.26.1
	gitlab.com/SyocR/tx_mmap v0.0.0-20220213110715-575fcf97527f
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd
	golang.org/x/sys v0.0.0-20220209214540-3681064d5158
)
