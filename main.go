package main

import (
	"flag"
	"golang.org/x/sys/unix"
	"io"
	"math"
	"os"
	"runtime"
	"strings"
	"sync"
	"time"
	"unsafe"

	//"github.com/Syoc/gopacket/pcap"
	"github.com/Syoc/gopacket/pcapgo"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/SyocR/replaycap/utils"
	"gitlab.com/SyocR/tx_mmap"
	"net/http"
	_ "net/http/pprof"
)
import "C"

type mmapFrame struct {
	dataPtr   unsafe.Pointer
	flagAvail unsafe.Pointer
	flagLen   unsafe.Pointer
}

func playPcap(path string, bpf string, writeData chan []byte) {
	f, err := os.Open(path)
	if err != nil {
		log.Fatal().Err(err).Msgf("Failed to open file %s", path)
	}
	defer f.Close()

	r, err := pcapgo.NewReader(f)
	if err != nil {
		log.Fatal().Err(err).Msgf("Failed to create reader for %s", path)
	}

	pkt := 0
	// Loop over packets and write them
	for {
		data, _, err := r.ReadPacketData()
		pkt++

		switch {
		case err == io.EOF:
			return
		case err != nil:
			log.Warn().Err(err).Msgf("Failed to read packet %d", pkt)
		default:
			writeData <- data
		}
		log.Trace().Int("length writeData", len(writeData)).Msg("")
	}
}

func meter(throughputMeter chan int, limit uint64) {
	start := time.Now()
	var bitsPerSecond uint64
	var total uint64
	logTicker := time.NewTicker(time.Second)

	for {
		select {
		case <-logTicker.C:
			bitsPerSecond = uint64(float64(total) / time.Since(start).Seconds())
			log.Info().Str("Throughput", utils.FormatThroughput(bitsPerSecond)).Msg("")
			log.Debug().Int("Space in channel", len(throughputMeter)).Msg("")
		case w := <-throughputMeter:
			total += uint64(w)
			bitsPerSecond = uint64(float64(total) / time.Since(start).Seconds())
			for limit != 0 && bitsPerSecond > limit && bitsPerSecond != 0 {
				time.Sleep(100 * time.Millisecond)
				bitsPerSecond = uint64(float64(total) / time.Since(start).Seconds())
			}
		}
	}
}

func pcap2Ptr(path string, meter chan int, pointers chan mmapFrame) {
	log.Debug().Str("Starting on pcap file", path).Msg("")
	f, err := os.Open(path)
	if err != nil {
		log.Fatal().Err(err).Msgf("Failed to open file %s", path)
	}
	defer f.Close()

	r, err := pcapgo.NewReader(f)
	if err != nil {
		log.Fatal().Err(err).Msgf("Failed to create reader for %s", path)
	}

	framePtr := mmapFrame{} // only allocate once
	var total uint64

	for {
		framePtr = <-pointers
		readBytes, err := r.ZCReadPacketToBuffer(framePtr.dataPtr)

		switch {
		case err == io.EOF:
			log.Debug().Str("Total bytes written", utils.HumanBits(total)).Msgf("file %s", path)
			return
		case err != nil:
			log.Warn().Err(err).Msg("Failed to read packet into buffer")
		default:
			(*(*int)(unsafe.Pointer(framePtr.flagAvail))) = unix.TP_STATUS_SEND_REQUEST
			(*(*C.uint)(unsafe.Pointer(framePtr.flagLen))) = C.uint(readBytes)
			total += uint64(readBytes)
		}
		meter <- readBytes * 8
	}
}

//func libpcapPacketWriter(iface string, data chan []byte, meter chan int, wg *sync.WaitGroup) {
//	// Open up a pcap handle for packet writes
//	defer wg.Done()
//
//	writeHandle, err := pcap.OpenLive(iface, 65536, true, pcap.BlockForever)
//	if err != nil {
//		log.Fatal().Err(err).Msg("PCAP OpenLive error (handle to write packet)")
//		return
//	}
//	defer writeHandle.Close()
//
//	var total uint64
//	for {
//		select {
//		case toWrite, open := <-data:
//			if !open {
//				log.Info().Str("Total bytes written", utils.HumanBits(total)).Msg("")
//				return
//			}
//			if err := writeHandle.WritePacketData(toWrite); err != nil {
//				log.Warn().Err(err).Msg("Failed to send packet")
//			}
//			meter <- len(toWrite) * 8
//			total += uint64(len(toWrite) * 8)
//		}
//	}
//}

func afpacketSend(tpacket *tx_mmap.TX_Mmap, terminator chan bool) {
	for {
		select {
		case _, open := <-terminator:
			if !open {
				return
			}
		default:
			sent := tpacket.Send()
			if sent == 0 {
				time.Sleep(time.Millisecond)
			}
			log.Trace().Int("Sent bytes", sent).Msg("")
		}
	}
}

func afpacketGetMmap(iface string, frameSize int, frameNum int, noUserSends bool) (tpacket *tx_mmap.TX_Mmap, err error) {
	ifaceOpt := tx_mmap.OptInterface(iface)
	userSendsOpt := tx_mmap.OptUserSends(!noUserSends)
	numFramesOpt := tx_mmap.OptNumFrames(math.Pow(2, float64(frameNum)))
	frameSizeOpt := tx_mmap.OptFrameSize(frameSize * 1024)
	tpacket, err = tx_mmap.NewTX_Mmap(ifaceOpt, userSendsOpt, numFramesOpt, frameSizeOpt)
	return
}

func afpacketFrameGetter(tpacket *tx_mmap.TX_Mmap, pointers chan mmapFrame, terminator chan bool, wg *sync.WaitGroup) {
	defer wg.Done()

	go afpacketSend(tpacket, terminator)

	for {
		select {
		case _, open := <-terminator:
			if !open {
				close(pointers)
				return
			}
		default:
			dataPtr, flagAvail, flagLen, err := tpacket.GetMmapBuffer()
			if err != nil {
				log.Error().Err(err).Msg("Failed to get frame from mmap")
				break
			}
			pointers <- mmapFrame{dataPtr: dataPtr, flagAvail: flagAvail, flagLen: flagLen}
		}
	}
}

func afpacketPacketWriter(tpacket *tx_mmap.TX_Mmap, data chan []byte, meter chan int, wg *sync.WaitGroup) {
	defer wg.Done()

	terminator := make(chan bool)
	go afpacketSend(tpacket, terminator)
	defer close(terminator)

	var total uint64
	for {
		toWrite, open := <-data
		if !open {
			log.Info().Str("Total bytes written", utils.HumanBits(total)).Msg("")
			return
		}
		if written, err := tpacket.WritePacketData(toWrite); err != nil {
			log.Error().Err(err).Msg("AFPacket WritePacketData error")
		} else {
			log.Debug().Int("Wrote to af_packet mmap", written).Msg("")
			total += uint64(written)
			meter <- written * 8
		}
	}
}

func main() {
	iface := flag.String("i", "eth0", "Interface to write packets to")
	fname := flag.String("single-file", "", "Filename to read from")
	stdin := flag.Bool("stdin", false, "Read pcap filenames from stdin, line seperated")
	bpf := flag.String("bpf", "", "BPF filter for written pcaps")
	repeat := flag.Bool("repeat", false, "Repeat pcap output in both single pcap mode and stdin mode")
	json := flag.Bool("json", false, "JSON log output")
	parallel := flag.Int("parallel", 10, "Number of pcaps that can be written to interface simultaneously in stdin mode")
	limit := flag.String("limit", "0", "Bits / seconds limit on throughput (number can end with K, M, G, T)")
	verbosity := flag.String("v", "info", "Verbosity (trace, debug, info, warning, error)")
	afpacket := flag.Bool("afpacket", false, "Use afpacket instead of libpcap for sending packets (faster)")
	afpacket_zc := flag.Bool("afpacket_zc", false, "Use afpacket and read pcap directly into mmap")
	profile := flag.Bool("profile", false, "Start pprof on port 6060")
	frameSize := flag.Int("frame-size", 8, "af_packet mmap frame size = 1024 x n")
	frameNum := flag.Int("frame-number", 15, "af_packet frame number = 2 ** n")
	noUserSends := flag.Bool("no-user-sends", false, "af_packet do not use a dedicated goroutine for sending")
	mmaps := flag.Int("send-mmaps", 1, "af_packet mmaps to allocate for sending")
	flag.Parse()

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if !*json {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.RFC3339})
	}
	utils.SetVerbosity(*verbosity)

	if *profile {
		runtime.SetBlockProfileRate(1)
		runtime.SetMutexProfileFraction(-1)
		go func() { // start webserver to get profiling info from
			log.Info().Msgf("%s", http.ListenAndServe("localhost:6060", nil))
		}()
	}

	// Sanity checks
	bitLimit, err := utils.SItoBits(*limit)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to parse --limit flag")
	} else {
		log.Debug().Msgf("Bitlimit parsed to %d", bitLimit)
	}
	if len(flag.Args()) > 0 {
		log.Fatal().Msgf("Unknown arguments", strings.Join(flag.Args(), " "))
	} else if *parallel <= 0 {
		log.Fatal().Msg("--parallel must be set to a positive integer")
	}

	var throughputMeter chan int
	if bitLimit != 0 {
		throughputMeter = make(chan int)
	} else {
		throughputMeter = make(chan int, 1000) // we don't really want to block...
	}
	go meter(throughputMeter, bitLimit)

	var wg sync.WaitGroup
	writeData := make(chan []byte, 1000)
	pointers := make(chan mmapFrame, 1000)
	terminator := make(chan bool)

	for i := 0; i < *mmaps; i++ {
		var tpacket *tx_mmap.TX_Mmap
		if *afpacket || *afpacket_zc {
			if tpacket, err = afpacketGetMmap(*iface, *frameSize, *frameNum, *noUserSends); err != nil {
				log.Fatal().Err(err).Msg("AFPacket NewTPacket error")
			}
			defer tpacket.Close()
		}

		if *afpacket {
			go afpacketPacketWriter(tpacket, writeData, throughputMeter, &wg)
		} else if *afpacket_zc {
			go afpacketFrameGetter(tpacket, pointers, terminator, &wg)
		} else {
			//go libpcapPacketWriter(*iface, writeData, throughputMeter, &wg)
		}
		wg.Add(1)
	}

	if *fname != "" && !*stdin {
		log.Info().Msg("Running in single pcap mode")
		if *afpacket_zc && *repeat {
			for {
				pcap2Ptr(*fname, throughputMeter, pointers)
			}
		} else if *afpacket_zc && !*repeat {
			pcap2Ptr(*fname, throughputMeter, pointers)
		} else if !*afpacket_zc && *repeat {
			for {
				playPcap(*fname, *bpf, writeData)
			}
		} else if !*afpacket_zc && !*repeat {
			playPcap(*fname, *bpf, writeData)
		}
	} else if *fname == "" && *stdin {
		log.Info().Msg("Running in stdin mode")
		log.Info().Msg("Write paths to pcap file, one on each line")
		guard := make(chan struct{}, *parallel)
		input := make(chan string, 5)
		paths := []string{}

		go utils.ReadStdin(input)

		for {
			if len(input) > 0 || !*repeat {
				path, open := <-input
				if !open && !*repeat {
					// we need to wait so our playPcap goroutines can finish
					for len(guard) != 0 {
						time.Sleep(100 * time.Millisecond)
					}
					break
				}
				paths = append(paths, path)
				guard <- struct{}{}
				if *afpacket_zc {
					go func(path string, bpf string, writeData chan []byte) {
						pcap2Ptr(path, throughputMeter, pointers)
						<-guard
					}(path, *bpf, writeData)
				} else {
					go func(path string, bpf string, writeData chan []byte) {
						pcap2Ptr(path, throughputMeter, pointers)
						<-guard
					}(path, *bpf, writeData)
				}
			} else if *repeat {
				for _, path := range paths {
					guard <- struct{}{}
					if *afpacket_zc {
						go func(path string, bpf string, writeData chan []byte) {
							pcap2Ptr(path, throughputMeter, pointers)
							<-guard
						}(path, *bpf, writeData)
					} else {
						go func(path string, bpf string, writeData chan []byte) {
							playPcap(path, bpf, writeData)
							<-guard
						}(path, *bpf, writeData)
					}
				}
			}
		}
		log.Info().Int("Total files processed", len(paths)).Msg("")
	} else {
		log.Fatal().Msg("Use either the --single-file flag or the --stdin flag to set input")
	}
	close(writeData)
	close(terminator)
	wg.Wait()
}
